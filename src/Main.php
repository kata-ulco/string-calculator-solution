<?php

declare(strict_types=1);

namespace Ulco;

use Ulco\Exception\NegativeNumberException;

use function count;
use function explode;
use function preg_match;
use function str_replace;
use function var_dump;

class Main
{
    private const MAX_HANDLED_NUMBER = 1000;

    /**
     * @throws NegativeNumberException
     */
    public function add(string $numbers): int
    {
        if ($numbers === '') {
            return 0;
        }

        $total = 0;

        $exoticDelimiter = $this->getDelimiter($numbers);

        $negatives = [];

        foreach (
            $this->extractNumbers(
                $this->getStringWithoutDelimiters($numbers),
                $exoticDelimiter
            ) as $explodedNumber
        ) {
            if ($explodedNumber >= self::MAX_HANDLED_NUMBER) {
                continue;
            }

            if ($explodedNumber < 0) {
                $negatives[] = $explodedNumber;
                continue;
            }

            $total += $explodedNumber;
        }

        if (count($negatives) > 0) {
            throw new NegativeNumberException($negatives);
        }

        return $total;
    }

    /**
     * @return int[]
     */
    private function extractNumbers(string $numbers, ?string $exoticDelimiter): array
    {
        $extractedNumbers = explode(',', $numbers);

        $finalNumbers = [];

        foreach ($extractedNumbers as $extractedNumber) {
            $splitByNewLine = explode('\\n', $extractedNumber);

            foreach ($splitByNewLine as $split) {
                if ($exoticDelimiter) {
                    $splitByExoticDelimiter = explode($exoticDelimiter, $split);
                    foreach ($splitByExoticDelimiter as $exoticResult) {
                        $finalNumbers[] = $exoticResult;
                    }
                } else {
                    $finalNumbers[] = $split;
                }
            }
        }

        return $finalNumbers;
    }

    private function getDelimiter(string $numbers): ?string
    {
        $matches = $this->match($numbers);

        return $matches[1] ?? null;
    }

    private function getStringWithoutDelimiters(string $numbers): string
    {
        return str_replace($this->match($numbers)[0] ?? '', '', $numbers);
    }

    private function match(string $numbers): array
    {
        preg_match('/\/\/\[?(.+?)]?\\\\n/', $numbers, $matches);

        return $matches;
    }
}