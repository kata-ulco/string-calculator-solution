<?php

declare(strict_types=1);

namespace Ulco\Exception;

use Exception;

use function implode;

class NegativeNumberException extends Exception
{
    public function __construct(array $negativeNumbers)
    {
        parent::__construct(sprintf('Negatives are not allowed, found : %s', implode(',', $negativeNumbers)));
    }
}