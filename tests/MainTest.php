<?php

declare(strict_types=1);

namespace Ulco\Tests;

use PHPUnit\Framework\TestCase;
use Ulco\Exception\NegativeNumberException;
use Ulco\Main;

class MainTest extends TestCase
{
    private ?Main $main = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->main = new Main();
    }

    public function testAddNumbersSumUp(): void
    {
        self::assertEquals(3, $this->main->add('1,2'));
    }

    public function testAddNumbersEmptyString(): void
    {
        self::assertEquals(0, $this->main->add(''));
    }

    public function testAddNumbersUnkAmountOfNumbers(): void
    {
        self::assertEquals(10, $this->main->add('1,2,3,4'));
    }

    public function testAddNumbersSeparatorNewLineAndComma(): void
    {
        self::assertEquals(6, $this->main->add('1,2\n3'));
    }

    public function testExoticDelimitersWithNewLine(): void
    {
        self::assertEquals(3, $this->main->add('//;\n1;2'));
    }

    public function testNegativeNumbers(): void
    {
        $this->expectException(NegativeNumberException::class);
        $this->expectExceptionMessage('Negatives are not allowed, found : -2,-3');

        self::assertEquals(3, $this->main->add('//;\n1;-2;-3'));
    }

    public function testBiggerNumberThan1000AreIgnored(): void
    {
        self::assertEquals(1, $this->main->add('//;\n1;1001'));
    }

    public function testDelimitersCanBeOfAnyLength(): void
    {
        self::assertEquals(6, $this->main->add('//[***]\n1***2***3'));
    }
}
